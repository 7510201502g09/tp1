package ar.fiuba.tdd.tp1;

import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class SpreadSheetTest {

    @Test
    public void testExistSpreadSheetTrue() {
        FiubaSpreadSheet excel = new SpreadSheet();
        excel.createBook("Book1");
        excel.createSheet("Book1", "Hoja1");
        assertTrue(excel.existSheet("Book1", "Hoja1"));
    }

    @Test
    public void testExistSpreadSheetFalse() {
        FiubaSpreadSheet excel = new SpreadSheet();
        excel.createBook("Book1");
        excel.createSheet("Book1", "Hoja1");
        assertTrue(!excel.existSheet("Book1", "Hoja99"));
    }

    @Test
    public void testDeleteSpreadSheet() {
        FiubaSpreadSheet excel = new SpreadSheet();
        excel.createBook("Book1");
        excel.createSheet("Book1", "Hoja1");
        excel.createSheet("Book1", "Hoja15");
        assertTrue(excel.existSheet("Book1", "Hoja15"));
        excel.deleteSheet("Book1", "Hoja15");
        assertTrue(!excel.existSheet("Book1", "Hoja15"));
    }

    @Test
    public void testSetGetCellSpreadSheet() {
        FiubaSpreadSheet excel = new SpreadSheet();
        excel.createBook("Book1");
        excel.createSheet("Book1", "Hoja1");
        excel.createSheet("Book1", "Hoja20");
        excel.setCell("Book1", "Hoja20", "A", 1, "3.0");
        String value = excel.getCell("Book1", "Hoja20", "A", 1);
        assertEquals("3.0", value);
    }

    @Test
    public void testSetGetCellSpreadSheet2() {
        FiubaSpreadSheet excel = new SpreadSheet();
        excel.createBook("Book1");
        excel.createSheet("Book1", "Hoja21");
        excel.setCell("Book1", "Hoja21", "A", 1, "4.0 + 5.0");
        String value = excel.getCell("Book1", "Hoja21", "A", 1);
        assertEquals("4.0 + 5.0", value);
    }

    @Test
    public void testSetGetCellSpreadSheet3() {
        FiubaSpreadSheet excel = new SpreadSheet();
        excel.createBook("Book1");
        excel.createSheet("Book1", "Hoja22");
        excel.setCell("Book1", "Hoja22", "A", 1, "2.0 + 7.0");
        String value = excel.getCellValue("Book1", "Hoja22", "A", 1);
        assertEquals("9.0", value);
    }

    @Test
    public void testUndoSpreadSheet() {
        FiubaSpreadSheet excel = new SpreadSheet();
        excel.createBook("Book1");
        excel.createSheet("Book1", "Hoja23");
        excel.setCell("Book1", "Hoja23", "A", 1, "4.0");
        excel.setCell("Book1", "Hoja23", "A", 1, "9.0");
        excel.undo();
        String value = excel.getCell("Book1", "Hoja23", "A", 1);
        assertEquals("4.0", value);
    }

    @Test
    public void testMultipleUndoSpreadSheet() {
        FiubaSpreadSheet excel = new SpreadSheet();
        excel.createBook("Book1");
        excel.createSheet("Book1", "Hoja24");
        excel.setCell("Book1", "Hoja24", "A", 1, "5.0");
        excel.setCell("Book1", "Hoja24", "A", 1, "2.0");
        excel.setCell("Book1", "Hoja24", "A", 1, "6.0");
        excel.setCell("Book1", "Hoja24", "A", 1, "4.0");
        excel.undo();
        String value = excel.getCell("Book1", "Hoja24", "A", 1);
        assertEquals("6.0", value);
        excel.undo();
        excel.undo();
        value = excel.getCell("Book1", "Hoja24", "A", 1);
        assertEquals("5.0", value);
    }

    @Test
    public void testUndoRedoSpreadSheet() {
        FiubaSpreadSheet excel = new SpreadSheet();
        excel.createBook("Book1");
        excel.createSheet("Book1", "Hoja25");
        excel.setCell("Book1", "Hoja25", "A", 1, "4.0");
        excel.setCell("Book1", "Hoja25", "A", 1, "9.0");
        excel.undo();
        String value = excel.getCell("Book1", "Hoja25", "A", 1);
        assertEquals("4.0", value);
        excel.redo();
        value = excel.getCell("Book1", "Hoja25", "A", 1);
        assertEquals("9.0", value);
    }

    @Test
    public void testMultipleUndoRedoSpreadSheet() {
        FiubaSpreadSheet excel = new SpreadSheet();
        excel.createBook("Book1");
        excel.createSheet("Book1", "Hoja26");
        excel.setCell("Book1", "Hoja26", "A", 1, "1.0");
        excel.setCell("Book1", "Hoja26", "A", 1, "2.0");
        excel.setCell("Book1", "Hoja26", "A", 1, "3.0");
        excel.setCell("Book1", "Hoja26", "A", 1, "4.0");
        excel.undo();
        String value = excel.getCell("Book1", "Hoja26", "A", 1);
        assertEquals("3.0", value);
        excel.undo();
        excel.undo();
        excel.redo();
        value = excel.getCell("Book1", "Hoja26", "A", 1);
        assertEquals("2.0", value);
        excel.redo();
        excel.redo();
        value = excel.getCell("Book1", "Hoja26", "A", 1);
        assertEquals("4.0", value);
    }

    @Test
    public void testOperationsSpreadSheet1() {
        SpreadSheet excel = new SpreadSheet();
        excel.createBook("Book1");
        excel.createSheet("Book1", "Hoja27");
        excel.setCell("Book1", "Hoja27", "A", 1, "1.0");
        excel.createSheet("Book1", "Hoja28");
        excel.setCell("Book1", "Hoja28", "B", 1, "1.0");
        excel.createSheet("Book1", "Hoja29");
        excel.setCell("Book1", "Hoja29", "C", 1, "=Hoja27!A1 + Hoja28!B1");
        String result = excel.getCellValue("Book1", "Hoja29", "C", 1);
        assertEquals("2.0", result);
    }

}