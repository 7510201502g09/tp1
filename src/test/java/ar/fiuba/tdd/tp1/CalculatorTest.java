package ar.fiuba.tdd.tp1;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class CalculatorTest {

    private static final double DELTA = 0.00001;

    @Test
    public void oneNumber() {
        assertEquals(5, Calculator.eval("5"), DELTA);
    }

    @Test
    public void sum() {
        assertEquals(7.5, Calculator.eval("5 + 2.5"), DELTA);
    }

    @Test
    public void multiSum() {
        assertEquals(11.5, Calculator.eval("0.5 + 2 + 4 + 5"), DELTA);
    }

    @Test
    public void sumWithNegative() {
        assertEquals(3, Calculator.eval("5 + -2"), DELTA);
    }

    @Test
    public void subtract() {
        assertEquals(1, Calculator.eval("3 - 2"), DELTA);
    }

    @Test
    public void multiSubtract() {
        assertEquals(-6, Calculator.eval("5 - 2 - 4 - 5"), DELTA);
    }

    @Test
    public void subtractWithNegative() {
        assertEquals(5, Calculator.eval("3 - -2"), DELTA);
    }

    @Test
    public void multiplication() {
        assertEquals(19.2, Calculator.eval("3.2 * 6"), DELTA);
    }

    @Test
    public void multiMultiplication() {
        assertEquals(200, Calculator.eval("5 * 2 * 4 * 5"), DELTA);
    }

    @Test
    public void multiplicationWithNegative() {
        assertEquals(-19.2, Calculator.eval("3.2 * -6"), DELTA);
    }

    @Test
    public void division() {
        assertEquals(18, Calculator.eval("36.0 / 2"), DELTA);
    }

    @Test
    public void multiDivision() {
        assertEquals(0.125, Calculator.eval("5 / 2 / 4 / 5"), DELTA);
    }

    @Test
    public void divisionWithNegative() {
        assertEquals(-18, Calculator.eval("-36 / 2"), DELTA);
    }

    @Test
    public void combineTwoOperatorsWithoutParentheses() {
        assertEquals(13, Calculator.eval("5 + 2 * 4"), DELTA);
    }

    @Test
    public void combineTwoOperatorsWithParentheses() {
        assertEquals(28, Calculator.eval("(5 + 2) * 4"), DELTA);
    }

    @Test
    public void combineMultipleOperatorsWithoutParentheses() {
        assertEquals(-1, Calculator.eval("1 + 2 * 3 - 4 * 5 + 6 * 2"), DELTA);
    }

    @Test
    public void combineMultipleOperatorsWithParentheses1() {
        assertEquals(-1, Calculator.eval("1 + (2 * 3) - (4 * 5) + (6 * 2)"), DELTA);
    }

    @Test
    public void combineMultipleOperatorsWithParentheses2() {
        assertEquals(-66, Calculator.eval("(1 + 2) * (3 - 4) * (5 + 6) * 2"), DELTA);
    }

    @Test
    public void combineMultipleOperatorsWithNestedParenthesis() {
        assertEquals(66, Calculator.eval("(1 + 2) * ((5 + 6) * 2)"), DELTA);
    }

    @Test
    public void combineMultipleOperatorsWithParenthesesBracketsBraces() {
        assertEquals(66, Calculator.eval("{(1 + 2) * [(5 + 6) * 2]}"), DELTA);
    }

    @Test
    public void combineOperatorsSamePrecedence1() {
        assertEquals(7 - 7, Calculator.eval("(5 + 4 - 2) - (5 - 2 + 4)"), DELTA);
    }

    @Test
    public void combineOperatorsSamePrecedence2() {
        assertEquals(10 - 10, Calculator.eval("(5 * 4 / 2) - (5 / 2 * 4)"), DELTA);
    }

    @Test
    public void combineOperatorsDifferentPrecedence1() {
        assertEquals(22 - 13, Calculator.eval("(5 * 4 + 2) - (5 + 2 * 4)"), DELTA);
    }

    @Test
    public void combineOperatorsDifferentPrecedence2() {
        assertEquals(6 - 15, Calculator.eval("(16 / 2 - 2) - (16 - 2 / 2)"), DELTA);
    }

    @Test(expected = IllegalArgumentException.class)
    public void emptyExpression() {
        Calculator.eval("");
    }

    @Test(expected = IllegalArgumentException.class)
    public void nullExpression() {
        Calculator.eval(null);
    }

    @Test(expected = IllegalArgumentException.class)
    public void invalidOperator() {
        Calculator.eval("5 # 3");
    }

    @Test(expected = IllegalArgumentException.class)
    public void multiInvalidOperators() {
        Calculator.eval("5 & 4 # 3");
    }

    @Test(expected = IllegalArgumentException.class)
    public void onlyOperators() {
        Calculator.eval("+ - *");
    }

    @Test(expected = IllegalArgumentException.class)
    public void incompleteExpression() {
        Calculator.eval("5 +");
    }

    @Test(expected = IllegalArgumentException.class)
    public void consecutiveOperators() {
        Calculator.eval("5 + * 2");
    }
}
