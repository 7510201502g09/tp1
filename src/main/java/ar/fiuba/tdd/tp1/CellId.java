package ar.fiuba.tdd.tp1;

public class CellId {

    private String column;
    private Integer row;

    public CellId(String inColumn, Integer inRow) {
        this.column = inColumn;
        this.row = inRow;
    }
    /*
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }

        CellId other = (CellId) obj;
        if (column.equals(other.column) && row.equals(other.row)) {
            return true;
        }
        return false;
    }*/
    @Override
    public boolean equals(Object obj) {
        if ((obj == null) || (getClass() != obj.getClass())) {
            return false;
        }

        CellId other = (CellId) obj;
        if (!column.equals(other.column) && (!row.equals(other.row))) {
            return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        return 17 * 31;
    }
}