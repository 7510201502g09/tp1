package ar.fiuba.tdd.tp1;

import java.lang.String;

public interface Worksheet {

    public void setCell(String column, Integer row, String value);

    public String getCell(String column, Integer row);

    public String getName();

    public boolean getTrue();
}