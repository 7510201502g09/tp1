package ar.fiuba.tdd.tp1;

import java.util.LinkedList;

public interface Operator {
    void calculate(LinkedList<Double> stack);
}
