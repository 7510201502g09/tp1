package ar.fiuba.tdd.tp1;
// Clase Calculator: ofrece el metodo publico eval(formula) que recibe un string
// con una formula algebraica numerica, y devuelve su resultado.
// Admite cinco operaciones: suma, resta, multiplicacion, division y modulo
// ( + - * / % ). Admite numeros enteros y decimales (con punto o coma). Acepta 
// parentesis, corchetes y llaves. En caso de error devuelve siempre la excepcion
// IllegalArgumentException.
// Proceso: transforma la formula recibida a formato RPN, y la resuelve de forma 
// similar al TP0

import java.util.*;

public class Calculator {

    private static Calculator ref;

    public static Calculator getCalculator() {
        if (ref == null) {
            ref = new Calculator();
        }
        return ref;
    }

    public static final Map<String, Operator> calculations = new HashMap<String, Operator>();

    static {
        calculations.put("+", (LinkedList<Double> stack) -> stack.push(stack.pop() + stack.pop()));
        calculations.put("-", (LinkedList<Double> stack) -> stack.push(-stack.pop() + stack.pop()));
        calculations.put("*", (LinkedList<Double> stack) -> stack.push(stack.pop() * stack.pop()));
        calculations.put("/", (LinkedList<Double> stack) -> stack.push((1 / stack.pop()) * stack.pop()));
        calculations.put("MOD", (LinkedList<Double> stack) -> {
                double first = stack.pop();
                stack.push(stack.pop() % first);
            });
    }

    public static double eval(String input) {
        Optional<String> opt = Optional.ofNullable(input);
        opt.filter(s -> !s.isEmpty())
                .orElseThrow(IllegalArgumentException::new);

        input = negatives(input);        // elimina numeros negativos
        input = symbols(input);         // reemplaza comas, corchetes y llaves
        input = spaces(input);            // normaliza espacios
        input = infixToPostfix(input);

        String next;
        LinkedList<Double> stack = new LinkedList<Double>();
        Scanner scan = new Scanner(input);
        while (scan.hasNext()) {
            next = scan.next();
            try {
                stack.push(Double.parseDouble(next));
            } catch (NumberFormatException n) {
                try {
                    calculations.get(next).calculate(stack);
                } catch (Exception e) {
                    throw new IllegalArgumentException();
                }
            }
        }
        return stack.pop();
    }
    //======================================================

    // verifica si un string es nulo o vacio
    private static boolean isNullOrEmpty(String str) {
        return str == null || str.isEmpty();
    }

    // reemplaza numeros negativos
    private static String negatives(String str) {
        return str.replaceAll("-([0-9]*\\.?[0-9]+)", "(0 - $1)");
    }

    // reemplaza comas, corchetes y llaves
    private static String symbols(String str) {
        return str.replaceAll("\\,", ".").replaceAll("\\[|\\{", "(").replaceAll("\\]|\\}", ")");
    }

    // normaliza espacios
    private static String spaces(String str) {
        return str.trim().replaceAll("([^0-9.,])", "$1 ").replaceAll("\\)", " )").replaceAll(" +", " ");
    }

    // verifica si un string representa un numero (entero o decimal)
    private static boolean isNumber(String str) {
        return str.matches("[-]?[0-9]*\\.?[0-9]+");
    }

    // verifica si un string representa un operador valido    
    private static boolean isOperator(String str) {
        return str.matches("[\\*|\\/|\\%|\\+|\\-]");
    }

    private enum Operators {
        ADD(1), SUBTRACT(2), MULTIPLY(3), DIVIDE(4);
        final int precedence;

        Operators(int preced) {
            precedence = preced;
        }
    }

    private static Map<String, Operators> ops = new HashMap<String, Operators>();

    static {
        ops.put("+", Operators.ADD);
        ops.put("-", Operators.SUBTRACT);
        ops.put("*", Operators.MULTIPLY);
        ops.put("/", Operators.DIVIDE);
    }

    private static boolean isHigerPrec(String op, String sub) {
        return (ops.containsKey(sub) && ops.get(sub).precedence >= ops.get(op).precedence);
    }

    private static String infixToPostfix(String infix) {

        StringBuilder output = new StringBuilder();
        Deque<String> stack = new LinkedList<>();

        for (String token : infix.split("\\s")) {
            inFuction(token, stack, output);
        }

        while (!stack.isEmpty()) {
            output.append(stack.pop()).append(' ');
        }
        return output.toString();
    }

    private static void inFuction(String token, Deque<String> stack, StringBuilder output) {
        if (ops.containsKey(token)) {
            while (!stack.isEmpty() && isHigerPrec(token, stack.peek())) {
                output.append(stack.pop()).append(' ');
            }
            stack.push(token);
        } else if (token.equals("(")) {
            stack.push(token);
        } else if (token.equals(")")) {
            while (!stack.peek().equals("(")) {
                output.append(stack.pop()).append(' ');
            }
            stack.pop();
        } else {
            output.append(token).append(' ');
        }
    }
}