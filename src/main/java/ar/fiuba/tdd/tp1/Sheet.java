package ar.fiuba.tdd.tp1;

import java.lang.Override;
import java.lang.String;
import java.util.HashMap;
import java.util.Map;

public class Sheet implements Worksheet {

    String name;
    private Map<CellId, String> cells = new HashMap<CellId, String>();

    public Sheet(String inputName) {
        this.name = inputName;
    }

    @Override
    public void setCell(String column, Integer row, String value) {
        CellId cellId = new CellId(column, row);
        cells.put(cellId, value);
    }

    @Override
    public String getCell(String column, Integer row) {
        CellId cellId = new CellId(column, row);
        return cells.get(cellId);
    }

    @Override
    public String getName() {
        return this.name;
    }

    public boolean getTrue() {
        return true;
    }
}