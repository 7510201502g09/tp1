package ar.fiuba.tdd.tp1;


public interface Operation {
    double run(double number1, double number2);
}