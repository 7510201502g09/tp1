package ar.fiuba.tdd.tp1;

public class SheetFactory extends WorksheetFactory {
    protected Worksheet factoryMethod(String name) {
        return new Sheet(name);
    }
}