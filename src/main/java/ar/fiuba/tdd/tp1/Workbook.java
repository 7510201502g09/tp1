package ar.fiuba.tdd.tp1;

public interface Workbook {

    public void createSheet(String sheetName);

    public void deleteSheet(String sheetName);

    public boolean existSheet(String sheetName);

    public void setCell(String sheetName, String column, Integer row, String value);

    public String getCell(String sheetName, String column, Integer row);

    public String getCellValue(String sheetName, String column, Integer row);
}
