package ar.fiuba.tdd.tp1;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class SpreadSheet implements FiubaSpreadSheet {

    private CommandManager commandMgr;
    private List<Book> books;

    public SpreadSheet() {
        commandMgr = new CommandManager();
        books = new ArrayList<Book>();
    }

    @Override
    public void createBook(String bookName) {
        books.add(new Book(bookName));
    }

    @Override
    public void deleteBook(String bookName) {
        books.removeIf(p -> p.getName().equals(bookName));
    }

    @Override
    public boolean existBook(String bookName) {
        return books.stream().anyMatch(p -> (p.getName()).equals(bookName));
    }

    @Override
    public void createSheet(String bookName, String sheetName) {
        Book book = null;
        book = books.stream().filter(p -> p.getName().equals(bookName)).findFirst().get();
        if (book != null) {
            book.createSheet(sheetName);
        }
    }

    @Override
    public void deleteSheet(String bookName, String sheetName) {
        Book book = null;
        book = books.stream().filter(p -> p.getName().equals(bookName)).findFirst().get();
        if (book != null) {
            book.deleteSheet(sheetName);
        }
    }

    @Override
    public boolean existSheet(String bookName, String sheetName) {
        Book book = null;
        book = books.stream().filter(p -> p.getName().equals(bookName)).findFirst().get();
        if (book != null) {
            return book.existSheet(sheetName);
        }
        return false;
    }

    @Override
    public void setCell(String bookName, String sheetName, String column, Integer row, String value) {
        Book book = null;
        book = books.stream().filter(p -> p.getName().equals(bookName)).findFirst().get();
        if (book != null) {
            Command command = new CommandCellSet(book, sheetName, column, row, value);
            this.commandMgr.executeCommand(command);
        }
    }

    @Override
    public String getCell(String bookName, String sheetName, String column, Integer row) {
        Book book = null;
        book = books.stream().filter(p -> p.getName().equals(bookName)).findFirst().get();
        if (book != null) {
            return book.getCell(sheetName, column, row);
        }
        return null;
    }

    @Override
    public String getCellValue(String bookName, String sheetName, String column, Integer row) {
        Book book = null;
        book = books.stream().filter(p -> p.getName().equals(bookName)).findFirst().get();
        if (book != null) {
            return book.getCellValue(sheetName, column, row);
        }
        return null;
    }

    @Override
    public void undo() {
        commandMgr.undo();
    }

    @Override
    public void redo() {
        commandMgr.redo();
    }
}