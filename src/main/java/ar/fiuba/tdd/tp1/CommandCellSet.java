package ar.fiuba.tdd.tp1;

import java.lang.Integer;
import java.lang.Override;
import java.lang.String;
import java.util.List;

public class CommandCellSet implements Command {

    private Book book;
    private String sheetName;
    private String column;
    private Integer row;
    private String value;
    private String valueBK;

    public CommandCellSet(Book inBook, String inSheetName, String inColumn, Integer inRow, String inValue) {

        this.book = inBook;
        this.sheetName = inSheetName;
        this.column = inColumn;
        this.row = inRow;
        this.value = inValue;
        this.valueBK = inValue;
    }

    @Override
    public void execute() {
        this.valueBK = book.getCell(this.sheetName, this.column, this.row);
        book.setCell(this.sheetName, this.column, this.row, this.value);
    }

    @Override
    public void undo() {
        book.setCell(this.sheetName, this.column, this.row, this.valueBK);
    }

    @Override
    public void redo() {
        // TODO no need to complete
    }

    public String getValueBK() {
        return this.valueBK;
    }
}