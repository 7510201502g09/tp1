package ar.fiuba.tdd.tp1;

import java.util.ArrayList;
import java.util.List;

public class Book implements Workbook {

    private String name;
    private List<Worksheet> sheets;

    private static Calculator calculator = Calculator.getCalculator();
    private static Parser parser = Parser.getParser();

    public Book(String inName) {
        this.name = inName;
        sheets = new ArrayList<Worksheet>();
    }

    @Override
    public void createSheet(String name) {
        if (!existSheet(name)) {
            WorksheetFactory creator = new SheetFactory();
            sheets.add(creator.makeSheet(name));
        }
    }

    @Override
    public void deleteSheet(String name) {
        sheets.removeIf(p -> p.getName().equals(name));
    }

    @Override
    public boolean existSheet(String name) {
        return sheets.stream().anyMatch(p -> (p.getName()).equals(name));
    }

    @Override
    public void setCell(String sheetName, String column, Integer row, String value) {
        Worksheet sheet = sheets.stream().filter(p -> p.getName().equals(sheetName)).findFirst().get();
        sheet.setCell(column, row, value);
    }

    @Override
    public String getCell(String sheetName, String column, Integer row) {
        Worksheet sheet = sheets.stream().filter(p -> p.getName().equals(sheetName)).findFirst().get();
        return sheet.getCell(column, row);
    }

    @Override
    public String getCellValue(String sheetName, String column, Integer row) {
        Worksheet sheet = sheets.stream().filter(p -> p.getName().equals(sheetName)).findFirst().get();
        String exp = sheet.getCell(column, row);
        if (exp.charAt(0) == '=') {
            exp = exp.replace("=", "");
            exp = parser.eval(this, exp);
        }
        return Double.toString(calculator.eval(exp));
    }

    public String getName() {
        return this.name;
    }
}