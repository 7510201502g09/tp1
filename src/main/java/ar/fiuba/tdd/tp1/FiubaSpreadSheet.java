package ar.fiuba.tdd.tp1;

// Interfaz a ser utilizada por el cliente

public interface FiubaSpreadSheet {

    public void createBook(String bookName);

    public void deleteBook(String bookName);

    public boolean existBook(String bookName);

    public void createSheet(String bookName, String sheetName);

    public void deleteSheet(String bookName, String sheetName);

    public boolean existSheet(String bookName, String sheetName);

    // setea un valor explicito en la celda
    public void setCell(String bookName, String sheetName, String column, Integer row, String value);

    // devuelve el valor explicito de la celda
    public String getCell(String bookName, String sheetName, String column, Integer row);

    // devuelve el valor calculado de la celda
    public String getCellValue(String bookName, String sheetName, String column, Integer row);

    // deshace la ultima operacion
    public void undo();

    // rehace la ultima operacion
    public void redo();
}