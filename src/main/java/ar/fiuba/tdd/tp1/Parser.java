package ar.fiuba.tdd.tp1;

public class Parser {

    private Parser() {
    }

    private static Parser instance;

    public static Parser getParser() {
        if (instance == null) {
            instance = new Parser();
        }
        return instance;
    }

    private static String numbers = "[-]?[0-9]*\\.?[0-9]+"; // Patron numeros validos
    private static String operations = "([+|\\-|*|/])";       // Patron operaciones validas
    private static String references = "\\w+\\![A-Z]+[0-9]"; //Patron referencias validas

    public String eval(Book book, String formula) throws IllegalArgumentException {
        //String str = formula.replaceAll(operations, " $1 ").replaceAll(" +", " ").trim();
        String str = formula.trim();
        String result = "";
        String result2 = "";
        StringBuffer buf = new StringBuffer();
        for (String token : str.split("\\s")) {
            if (isNumber(token)) {
                buf.append(token);
            } else if (isOperation(token)) {
                buf.append(" " + token + " ");
            } else if (isReference(token)) {
                result2 = getReference(book, token);
                buf.append(result2);
            } else {
                throw new IllegalArgumentException();
            }
        }
        result = buf.toString();
        return result;
    }

    private static boolean isNumber(String token) {
        return token.matches(numbers);
    }

    private static boolean isOperation(String token) {
        return token.matches(operations);
    }

    private static boolean isReference(String token) {
        return token.matches(references);
    }

    private static String getReference(Book book, String token) {
        String[] parts = token.split("!");
        String sheetName = parts[0];
        String colRow = parts[1];
        String result = "";
        if (book.existSheet(sheetName)) {
            String[] secuen = colRow.split("[A-Z]+");
            int row = Integer.parseInt(secuen[1]);
            secuen = colRow.split("[0-9]+");
            String column = secuen[0];
            result = book.getCellValue(sheetName, column, row);
        }
        return result;
    }
}