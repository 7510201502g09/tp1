package ar.fiuba.tdd.tp1;

public abstract class WorksheetFactory {

    protected abstract Worksheet factoryMethod(String name);

    public Worksheet makeSheet(String name) {
        Worksheet worksheet = factoryMethod(name);
        return worksheet;
    }
}