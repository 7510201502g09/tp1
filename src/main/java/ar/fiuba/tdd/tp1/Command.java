package ar.fiuba.tdd.tp1;

public interface Command {

    public void execute();

    public void undo();

    public void redo();
}